# Vendor Accruals Setup Guide #

### How do I get set up? ###

1. Clone vendor accruals [repo](https://bitbucket.org/gaapapps/vendor-accruals/)
2. Checkout to main branch
3. Run `composer install`
4. Create `.env` to the root of the repo
5. Copy the contents of [`.env.example`](.env.example) and paste to your `.env`
6. Setup database connections for `.env`
    ```
    MAIN_DB_DATABASE=
    MAIN_DB_USERNAME=root
    MAIN_DB_PASSWORD=

    DB80_DB_DATABASE=
    DB80_DB_USERNAME=root
    DB80_DB_PASSWORD=

    DB118_DB_DATABASE=
    DB118_DB_USERNAME=root
    DB118_DB_PASSWORD=
    ```
7. Run `php artisan key:generate`
8. Setup `APP_URL` for `.env`
9.  Setup TESTs keys for `.env` and `.env.dusk.local`
    ```
    TEST_TRIGGER_URL=true
    TEST_CONNECTION=
    TEST_TOTAL_CSV_ROWS=7
    TEST_EMAIL=
    TEST_CUSTOMER_ID=
    ```
10. Setup database `API_KEY` for `.env`
11. Run migration and seeders
    1. `php artisan migrate`
    2. `php artisan db:seed --class=SourcePurchaseOrderSeeder`
    3. `php artisan db:seed --class=VendorTaskSeeder`
12. Run `php artisan test` to test your setup

### Contribution guidelines ###

* Should I miss any, feel free to submit a pull request.

### Who do I talk to? ###

* Repo owner or admin
* PH Dev Team group chat

----
#### Footnotes: ###
*  hc_qa and hc_qa_2 Databases download [link](https://drive.google.com/file/d/1oazMxr_XYPH2TqU4mc5lC_9Dq93ulC-8/view)
